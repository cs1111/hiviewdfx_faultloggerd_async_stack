/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef UNIQUE_STACK_TABLE_H
#define UNIQUE_STACK_TABLE_H

#include <cinttypes>
#include <cstdint>
#include <mutex>
#include <memory>
#include <string>
#include <vector>

#include <sys/mman.h>

namespace OHOS {
namespace HiviewDFX {
namespace {
#define ADDR_BIT_LENGTH        40
#define IDX_BIT_LENGTH         23
#define DECONFLICT_INCREASE_STEP  3
#define RESIZE_MULTIPLE          2
#define NR_BIT_LENGTH          41
constexpr uint32_t INITIAL_TABLE_SIZE = 1 * 1024 * 1024;
constexpr uint32_t MAX_NODES_CNT = 1 << IDX_BIT_LENGTH ;
constexpr uint64_t HEAD_NODE_INDEX = 0;
constexpr uint8_t INIT_DECONFLICT_ALLOWED = 22;
}

// align
#pragma pack(push, 4)

union Node {
    uint64_t value;
    struct {
        uint64_t pc : ADDR_BIT_LENGTH;
        uint64_t prevIdx : IDX_BIT_LENGTH;
    } section;
};

union StackId {
    uint64_t value;
    struct {
        uint64_t id : IDX_BIT_LENGTH;
        uint64_t nr : NR_BIT_LENGTH;
    } section;
};

#pragma pack(pop)
static_assert(sizeof(Node) == 8, "Node size must be 8 byte");

class UniqueStackTable {
public:
    bool Init();
    static UniqueStackTable* GetInstance();
    static void DeleteInstance();
    uint64_t PutPcsInTable(StackId *stackId, uintptr_t *pcs, int64_t nr);
    bool GetPcsByStackId(const StackId& stackId, std::vector<uintptr_t>& pcs);
    bool ImportNode(uint32_t index, const Node& node);
    size_t GetWriteSize();

    bool Resize();

    void SetTableBuf(void* tableBufMMap, uint32_t tableSize)
    {
        tableBufMMap_ = tableBufMMap;
        tableSize_ = tableSize;
    }

    uint32_t GetTabelSize()
    {
        return tableSize_;
    }

    const std::vector<uint32_t>& GetUsedIndexes()
    {
        return usedSlots_;
    }

    const Node* GetHeadNode()
    {
        return reinterpret_cast<Node *>(tableBufMMap_);
    }
private:
    UniqueStackTable() {};
    ~UniqueStackTable();
    UniqueStackTable(const UniqueStackTable& stackTable);

    const UniqueStackTable &operator = (const UniqueStackTable &stackTable) = delete;
    Node* GetFrame(uint64_t stackId);
    uint64_t PutPcInSlot(uint64_t thisPc, uint64_t prevIdx);
    void* tableBufMMap_ = nullptr;
    uint32_t tableSize_ = INITIAL_TABLE_SIZE;
    std::vector<uint32_t> usedSlots_ {0};
    uint32_t totalNodes_ = 0;
    // current available node count, include index 0
    uint32_t availableNodes_ = 0;
    uint32_t hashModulus_ = 0;
    // 0 for reserved, start from 1
    uint32_t availableIndex_ = 1;
    // for de-conflict
    uint64_t hashStep_ = 0;
    uint8_t deconflictTimes_ = INIT_DECONFLICT_ALLOWED;
    static std::mutex stackTableMutex_;
    static UniqueStackTable* instance_;
};
}
}
#endif // UNIQUE_STACK_TABLE_H