# Copyright (c) 2022-2024 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/hiviewdfx/faultloggerd/faultloggerd.gni")
import("//build/config/features.gni")
import("//build/test.gni")

module_output_path = "faultloggerd/backtrace"

ohos_unittest("backtrace_utils_test") {
  configs = [ "//third_party/jsoncpp:jsoncpp_public_config" ]
  module_out_path = module_output_path
  visibility = [ "*:*" ]
  include_dirs = [
    "$faultloggerd_interfaces_path/common",
    "$faultloggerd_path/test/utils",
  ]
  sources = [ "backtrace_utils_test.cpp" ]
  deps = [
    "$faultloggerd_common_path/dfxutil:dfx_util",
    "$faultloggerd_interfaces_path/innerkits/backtrace:libbacktrace_local",
    "$faultloggerd_path/test/utils:dfx_test_util",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
    "//third_party/libunwind:libunwind",
  ]
  if (use_musl && use_jemalloc && use_jemalloc_dfx_intf) {
    defines = [ "USE_JEMALLOC_DFX_INTF" ]
  }
  branch_protector_ret = "pac_ret"
  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("backtrace_local_test") {
  configs = [ "//third_party/jsoncpp:jsoncpp_public_config" ]
  module_out_path = module_output_path
  visibility = [ "*:*" ]
  include_dirs = [
    "$faultloggerd_interfaces_path/common",
    "$faultloggerd_path/test/utils",
    "$faultloggerd_frameworks_path/unwind/include",
    "$faultloggerd_interfaces_path/innerkits/backtrace",
  ]
  sources = [ "backtrace_local_test.cpp" ]
  deps = [
    "$faultloggerd_common_path/dfxutil:dfx_util",
    "$faultloggerd_interfaces_path/innerkits/backtrace:libbacktrace_local",
    "$faultloggerd_path/test/utils:dfx_test_util",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
    "//third_party/libunwind:libunwind",
  ]
  branch_protector_ret = "pac_ret"
  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("catchframe_local_test") {
  configs = [ "//third_party/jsoncpp:jsoncpp_public_config" ]
  module_out_path = module_output_path
  visibility = [ "*:*" ]
  include_dirs = [
    "$faultloggerd_interfaces_path/common",
    "$faultloggerd_path/test/utils",
  ]
  sources = [ "catchframe_local_test.cpp" ]
  deps = [
    "$faultloggerd_path/common/dfxutil:dfx_util",
    "$faultloggerd_path/interfaces/innerkits/backtrace:libbacktrace_local",
    "$faultloggerd_path/test/utils:dfx_test_util",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
    "//third_party/libunwind:libunwind",
  ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]
}

ohos_unittest("backtrace_utils_test_static") {
  configs = [ "//third_party/jsoncpp:jsoncpp_public_config" ]
  module_out_path = module_output_path
  visibility = [ "*:*" ]
  include_dirs = [
    "$faultloggerd_interfaces_path/common",
    "$faultloggerd_path/test/utils",
    "$faultloggerd_frameworks_path/unwind/include",
    "$faultloggerd_interfaces_path/innerkits/backtrace",
    "//third_party/libunwind/include",
    "//third_party/libunwind/include/tdep/",
    "//third_party/libunwind/src",
  ]
  defines = [ "DFX_LOG_HILOG_BASE" ]
  sources = [ "backtrace_utils_test.cpp" ]
  deps = [
    "$faultloggerd_common_path/dfxutil:dfx_util_static",
    "$faultloggerd_interfaces_path/innerkits/backtrace:backtrace_local",
    "$faultloggerd_path/test/utils:dfx_test_util",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
  ]
  if (use_musl && use_jemalloc && use_jemalloc_dfx_intf) {
    defines += [ "USE_JEMALLOC_DFX_INTF" ]
  }
  branch_protector_ret = "pac_ret"
  external_deps = [
    "c_utils:utils",
    "hilog:libhilog_base",
  ]
}

ohos_unittest("backtrace_local_test_static") {
  configs = [ "//third_party/jsoncpp:jsoncpp_public_config" ]
  module_out_path = module_output_path
  visibility = [ "*:*" ]
  include_dirs = [
    "$faultloggerd_interfaces_path/common",
    "$faultloggerd_path/test/utils",
    "$faultloggerd_frameworks_path/unwind/include",
    "$faultloggerd_interfaces_path/innerkits/backtrace",
    "//third_party/libunwind/include",
    "//third_party/libunwind/include/tdep/",
    "//third_party/libunwind/src",
  ]
  defines = [ "DFX_LOG_HILOG_BASE" ]
  sources = [ "backtrace_local_test.cpp" ]
  deps = [
    "$faultloggerd_common_path/dfxutil:dfx_util_static",
    "$faultloggerd_interfaces_path/innerkits/backtrace:backtrace_local",
    "$faultloggerd_path/test/utils:dfx_test_util",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
    "//third_party/libunwind:libunwind",
  ]
  branch_protector_ret = "pac_ret"
  external_deps = [
    "c_utils:utils",
    "hilog:libhilog_base",
  ]
}

ohos_unittest("catchframe_local_test_static") {
  configs = [ "//third_party/jsoncpp:jsoncpp_public_config" ]
  module_out_path = module_output_path
  visibility = [ "*:*" ]
  include_dirs = [
    "$faultloggerd_interfaces_path/common",
    "$faultloggerd_path/test/utils",
    "$faultloggerd_frameworks_path/unwind/include",
    "//third_party/libunwind/include",
    "//third_party/libunwind/include/tdep/",
    "//third_party/libunwind/src",
  ]
  defines = [ "DFX_LOG_HILOG_BASE" ]
  sources = [ "catchframe_local_test.cpp" ]
  deps = [
    "$faultloggerd_path/common/dfxutil:dfx_util_static",
    "$faultloggerd_path/interfaces/innerkits/backtrace:backtrace_local",
    "$faultloggerd_path/test/utils:dfx_test_util",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
  ]
  external_deps = [
    "c_utils:utils",
    "hilog:libhilog_base",
  ]
}

group("unittest") {
  testonly = true
  deps = [
    ":backtrace_local_test",
    ":backtrace_local_test_static",
    ":backtrace_utils_test",
    ":backtrace_utils_test_static",
    ":catchframe_local_test",
    ":catchframe_local_test_static",
  ]
}
