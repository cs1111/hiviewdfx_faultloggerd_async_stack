# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/hiviewdfx/faultloggerd/faultloggerd.gni")

if (defined(ohos_lite)) {
  import("//build/lite/config/component/lite_component.gni")
  import("//build/lite/config/test.gni")

  if (ohos_build_type == "debug") {
    unittest("test_processdump") {
      output_extension = "bin"
      output_dir = "$root_out_dir/test/unittest/faultloggerd"

      include_dirs = [
        "$c_utils_include_path",
        "$faultloggerd_common_path/dfxutil",
        "$faultloggerd_interfaces_path/common",
        "$faultloggerd_interfaces_path/innerkits/procinfo/include",
        "$faultloggerd_interfaces_path/innerkits/unwinder/include",
        "$faultloggerd_path/test/utils",
        "$faultloggerd_path/tools/process_dump",
        "//third_party/googletest/googlemock/include",
        "$hilog_lite_include_path",
      ]

      sources = [
        "dfx_processdump_test.cpp",
        "multithread_constructor.c",
        "process_dump_test.cpp",
      ]
      sources += [ "$c_utils_src_path/directory_ex.cpp" ]

      deps = [
        "$faultloggerd_interfaces_path/innerkits/procinfo:libdfx_procinfo",
        "$faultloggerd_interfaces_path/innerkits/unwinder:libunwinder",
        "$faultloggerd_path/test/utils:dfx_test_util",
        "$faultloggerd_path/tools/process_dump:process_info_src",
        "//base/hiviewdfx/hilog_lite/frameworks/featured:hilog_shared",
      ]
    }
  }

  group("unittest") {
    deps = [ ":test_processdump" ]
  }
} else {
  import("//build/config/features.gni")
  import("//build/test.gni")

  config("module_private_config") {
    visibility = [ ":*" ]

    include_dirs = [
      "$faultloggerd_common_path/dfxutil",
      "$faultloggerd_interfaces_path/common",
      "$faultloggerd_interfaces_path/innerkits/unwinder/include",
      "$faultloggerd_path/test/utils",
      "$faultloggerd_path/tools/process_dump",
    ]
  }

  module_output_path = "faultloggerd/process_dump"

  ohos_unittest("test_processdump") {
    module_out_path = module_output_path
    sources = [
      "dfx_processdump_test.cpp",
      "multithread_constructor.c",
      "process_dump_test.cpp",
    ]

    configs = [
      ":module_private_config",
      "$faultloggerd_path/common/build:coverage_flags",
      "//third_party/jsoncpp:jsoncpp_public_config",
    ]

    defines = [ "UNITTEST" ]

    deps = [
      "$faultloggerd_path/interfaces/innerkits/unwinder:libunwinder_static",
      "$faultloggerd_path/test/utils:dfx_test_util",
      "$faultloggerd_path/tools/process_dump:process_info_src",
      "//third_party/googletest:gmock_main",
      "//third_party/googletest:gtest_main",
      "//third_party/jsoncpp:jsoncpp",
    ]
    external_deps = [ "c_utils:utils" ]
    if (support_jsapi) {
      deps += [ "$faultloggerd_path/test/resource:FaultloggerdJsTest" ]
    }
    resource_config_file = "$faultloggerd_path/test/resource/ohos_test.xml"
  }

  ohos_unittest("test_faultstack") {
    module_out_path = module_output_path
    sources = [ "fault_stack_test.cpp" ]

    configs = [
      ":module_private_config",
      "$faultloggerd_path/common/build:coverage_flags",
      "//third_party/jsoncpp:jsoncpp_public_config",
    ]

    defines = [ "UNITTEST" ]

    deps = [
      "$faultloggerd_path/interfaces/innerkits/unwinder:libunwinder_static",
      "$faultloggerd_path/tools/process_dump:process_info_src",
      "//third_party/googletest:gmock_main",
      "//third_party/googletest:gtest_main",
      "//third_party/jsoncpp:jsoncpp",
    ]

    external_deps = [ "c_utils:utils" ]
  }

  group("unittest") {
    testonly = true
    deps = [
      ":test_faultstack",
      ":test_processdump",
    ]
  }
}
